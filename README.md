                                                                    
**   AC Valhalla Multiple Save Game **  

**YOU MUST DISABLE UBI CLOUD SYNC**

** https://support.ubisoft.com/en-US/faqs/000025873     **                         

**YOU MUST SET UBICONNECT TO OFFLINE MODE**
** https://support.ubisoft.com/en-US/faqs/000025871#:~:text=If%20you%20wish%20to%20launch,logged%20into%20the%20app%20before.**

                                                                   
   This script will copy saved game data to an alternate            
   location to allow the use of simultaneous "profiles"             
   in AC Valhalla                                                   
                                                                    
   Usage:                                                           
   Run ValhallaProfileSwitcher.ps1 -profilename[profile1][profile2]
   Or right click > Run with powershell  
                                                                    
   Selecting a profile will move save data to a temporary           
   location while copying the alternate saves to the original       
   AC Valhalla save location. Running the script again with         
   The second profile will reverse the action.                      

   If your user directory is on something other than C: 
   change the path variable for ProfileLocation and SaveLocation              
                                                                  


   Execution policy must be set on bypass.                          
   ps> set-executionpolicy -bypass                                  
                                                                    
   Version 0.4 (this could really use validation and try/catch      
   USE THIS AT YOUR OWN RISK. COPY YOUR SAVE DATA SOMEWHERE FIRST   
   Created by DJEtaine                                              
                                                                    