####################################################################
#  AC Valhalla Multiple Save Game                                  #
#                                                                  #
#  This script will copy saved game data to an alternate           #
#  location to allow the use of simultaneous "profiles"            #
#  in AC Valhalla                                                  #
#  PreRequisites: CLOUD SYNC MUST BE DISABLED. SEE THE LINK BELOW  #
#  https://support.ubisoft.com/en-US/faqs/000025873                #
#  You MUST HAVE UBICONNECT IN OFFLINE MODE                        #
#  Usage:                                                          #
#  Run ValhallaProfileSwitcher.ps1 -profilename[profile1][profile2]#
#                                                                  #
#  Selecting a profile will move save data to a temporary          #
#  location while copying the alternate saves to the original      #
#  AC Valhalla save location. Running the script again with        #
#  The second profile will reverse the action.                     #
#                                                                  #
#  If your user directory is on something other than C: change the #
#  path variable for ProfileLocation and SaveLocation              #
#                                                                  #
#  Execution policy must be set on bypass.                         #
#  ps> set-executionpolicy -bypass                                 #
#                                                                  #
#  Version 0.5 (this could really use validation and try/catch     #
#  USE THIS AT YOUR OWN RISK. COPY YOUR SAVE DATA SOMEWHERE FIRST  #
#  Created by DJEtaine                                             #
####################################################################

#Check for admin rights and relaunch if not (this must be run as admin to modify the program files directory
If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))
{
  # Relaunch as an elevated process:
  Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
  exit
}

#Assign shell arguments
param ($ProfileName)
if ($ProfileName -eq $null) {
$ProfileName = read-host -Prompt "Enter the Profile you want to play as (Profile1 or Profile2)"}

#Assign variables for profile sets
if ($ProfileName -eq 'Profile1') {
$AltProfile = 'Profile2'}
if ($ProfileName -eq 'Profile2') {
$AltProfile = 'Profile1'}

#Define Location of AC Valhalla save directory and get random GUID
$ProfileLocation = 'C:\Program Files (x86)\Ubisoft\Ubisoft Game Launcher\'
$SaveLocation = 'C:\Program Files (x86)\Ubisoft\Ubisoft Game Launcher\savegames'
$SaveGuid = Get-ChildItem $SaveLocation |select name -ExpandProperty name


#Test if this is the first time the script has been run
#Checks to see if the 'Profile2' Directory has been created and creates if not'
if (!(Test-Path "$ProfileLocation\Profile2")) 
{
$FirstTime = 'Yes'
New-Item "$ProfileLocation\Profile1" -ItemType Directory -Force
New-Item "$ProfileLocation\Profile2" -ItemType Directory -Force

#Copy initial save data to Profile 1 store
Copy-Item -Recurse $SaveLocation\$SaveGuid\13504\*.* $ProfileLocation\Profile1\

#Clear out save data from default location to make way for the second profile saves
Remove-item $SaveLocation\$SaveGuid\13504\*.*}

#If the directories exist already, go on to copy save data from default to store directory
if ($Firsttime -ne 'Yes') {
Copy-Item -Recurse $SaveLocation\$SaveGuid\13504\*.* $ProfileLocation$AltProfile
#Clean up the default save dir
Remove-item $SaveLocation\$SaveGuid\13504\*.*
#Copy the requested save data into the default game save dir
Copy-Item -Recurse $ProfileLocation$ProfileName\*.* $SaveLocation\$SaveGuid\13504\}

